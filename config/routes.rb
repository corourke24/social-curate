Rails.application.routes.draw do
 
  root 'static_pages#home'
  get '/contact',          to: 'messages#new', as: 'new_message'
  post '/contact',         to: 'messages#create', as: 'create_message'
  
  get '/test', to: 'static_pages#test'
end

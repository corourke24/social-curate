// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require bootstrap/modal
//= require jquery.flexslider
//= require skrollr
//= require skrollr.ie
//= require skrollr.menu
//= require skrollr.stylesheets
//= require_tree .

/*global $*/
$(document).on("turbolinks:load", function() {
    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: false,
        directionNav: true,
        animationLoop: true,
        touch: true,
        slideshowSpeed: 5000,
  });
});

/* Smooth Scrolling */

$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});


/*Side Down div */

$(document).ready(function(){
    $(".slider").click(function(){
     var link = $(this);      
     link.closest('.slider').find('.hidden-div').slideToggle("slow");
    });
});

/* Button sequence "Secret Page" */

var backgrounds = ["red", "green", "yellow", "orange", "purple", "violet", "pink", "steelblue", "blue"]; 
function setColor(el){ 
    el.colorIdx = el.colorIdx || 0; 
    el.style.backgroundColor = backgrounds[el.colorIdx++ % backgrounds.length]; 
}

/* Secret Button */

var buttonMessage = "Good day";
function buttonCheck() {
    if ($('#div1').css('background-color')=='rgb(255, 255, 255') {
        buttonMessage = "yes";
    } else {
        buttonMessage = "Good evening";
    }
    document.getElementById("demo").innerHTML = buttonMessage;
}